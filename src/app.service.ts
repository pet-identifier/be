import { Injectable } from '@nestjs/common';
import {
  communes,
  countries,
  districts,
  provinces,
  species,
  tribes,
} from './data';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  private transformResponse(data: any) {
    return { data: data ? data : [], statusCode: 200 };
  }

  getProvinces() {
    return this.transformResponse(provinces);
  }

  getCountries() {
    return this.transformResponse(countries);
  }

  getSpecies() {
    return this.transformResponse(species);
  }

  getTribes(code: number) {
    return this.transformResponse(tribes[code]);
  }

  getDistricts(code: number) {
    return this.transformResponse(districts[code]);
  }

  getCommune(code: number) {
    return this.transformResponse(communes[code]);
  }
}
