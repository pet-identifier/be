import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/provinces')
  provinces() {
    return this.appService.getProvinces();
  }

  @Get('/countries')
  countries() {
    return this.appService.getCountries();
  }

  @Get('/species')
  species() {
    return this.appService.getSpecies();
  }

  @Get('/tribes')
  tribes(@Query('specie_code') specieCode: number) {
    return this.appService.getTribes(specieCode);
  }

  @Get('/districts')
  districts(@Query('province_id') code: number) {
    return this.appService.getDistricts(code);
  }

  @Get('/communes')
  communes(@Query('district_id') code: number) {
    return this.appService.getCommune(code);
  }
}
