export const provinces = [
  {
    province_id: 1,
    name: 'Hà Nội',
  },
  {
    province_id: 79,
    name: 'Hồ Chí Minh',
  },
  {
    province_id: 89,
    name: 'An Giang',
  },
  {
    province_id: 77,
    name: 'Bà Rịa - Vũng Tàu',
  },
  {
    province_id: 24,
    name: 'Bắc Giang',
  },
  {
    province_id: 6,
    name: 'Bắc Kạn',
  },
  {
    province_id: 95,
    name: 'Bạc Liêu',
  },
  {
    province_id: 27,
    name: 'Bắc Ninh',
  },
  {
    province_id: 83,
    name: 'Bến Tre',
  },
  {
    province_id: 74,
    name: 'Bình Dương',
  },
  {
    province_id: 52,
    name: 'Bình Định',
  },
  {
    province_id: 70,
    name: 'Bình Phước',
  },
  {
    province_id: 60,
    name: 'Bình Thuận',
  },
  {
    province_id: 96,
    name: 'Cà Mau',
  },
  {
    province_id: 92,
    name: 'Cần Thơ',
  },
  {
    province_id: 4,
    name: 'Cao Bằng',
  },
  {
    province_id: 48,
    name: 'Đà Nẵng',
  },
  {
    province_id: 66,
    name: 'Đắk Lắk',
  },
  {
    province_id: 67,
    name: 'Đắk Nông',
  },
  {
    province_id: 11,
    name: 'Điện Biên',
  },
  {
    province_id: 75,
    name: 'Đồng Nai',
  },
  {
    province_id: 87,
    name: 'Đồng Tháp',
  },
  {
    province_id: 64,
    name: 'Gia Lai',
  },
  {
    province_id: 2,
    name: 'Hà Giang',
  },
  {
    province_id: 35,
    name: 'Hà Nam',
  },
  {
    province_id: 42,
    name: 'Hà Tĩnh',
  },
  {
    province_id: 30,
    name: 'Hải Dương',
  },
  {
    province_id: 31,
    name: 'Hải Phòng',
  },
  {
    province_id: 93,
    name: 'Hậu Giang',
  },
  {
    province_id: 17,
    name: 'Hòa Bình',
  },
  {
    province_id: 33,
    name: 'Hưng Yên',
  },
  {
    province_id: 56,
    name: 'Khánh Hòa',
  },
  {
    province_id: 91,
    name: 'Kiên Giang',
  },
  {
    province_id: 62,
    name: 'Kon Tum',
  },
  {
    province_id: 12,
    name: 'Lai Châu',
  },
  {
    province_id: 68,
    name: 'Lâm Đồng',
  },
  {
    province_id: 20,
    name: 'Lạng Sơn',
  },
  {
    province_id: 10,
    name: 'Lào Cai',
  },
  {
    province_id: 80,
    name: 'Long An',
  },
  {
    province_id: 36,
    name: 'Nam Định',
  },
  {
    province_id: 40,
    name: 'Nghệ An',
  },
  {
    province_id: 37,
    name: 'Ninh Bình',
  },
  {
    province_id: 58,
    name: 'Ninh Thuận',
  },
  {
    province_id: 25,
    name: 'Phú Thọ',
  },
  {
    province_id: 54,
    name: 'Phú Yên',
  },
  {
    province_id: 44,
    name: 'Quảng Bình',
  },
  {
    province_id: 49,
    name: 'Quảng Nam',
  },
  {
    province_id: 51,
    name: 'Quảng Ngãi',
  },
  {
    province_id: 22,
    name: 'Quảng Ninh',
  },
  {
    province_id: 45,
    name: 'Quảng Trị',
  },
  {
    province_id: 94,
    name: 'Sóc Trăng',
  },
  {
    province_id: 14,
    name: 'Sơn La',
  },
  {
    province_id: 72,
    name: 'Tây Ninh',
  },
  {
    province_id: 34,
    name: 'Thái Bình',
  },
  {
    province_id: 19,
    name: 'Thái Nguyên',
  },
  {
    province_id: 38,
    name: 'Thanh Hóa',
  },
  {
    province_id: 46,
    name: 'Thừa Thiên Huế',
  },
  {
    province_id: 82,
    name: 'Tiền Giang',
  },
  {
    province_id: 84,
    name: 'Trà Vinh',
  },
  {
    province_id: 8,
    name: 'Tuyên Quang',
  },
  {
    province_id: 86,
    name: 'Vĩnh Long',
  },
  {
    province_id: 26,
    name: 'Vĩnh Phúc',
  },
  {
    province_id: 15,
    name: 'Yên Bái',
  },
];

export const species = [
  {
    id: 9,
    specie_code: 9,
    name: 'Chó',
  },
  {
    id: 8,
    specie_code: 8,
    name: 'Mèo',
  },
  {
    id: 7,
    specie_code: 7,
    name: 'Chim',
  },
  {
    id: 6,
    specie_code: 6,
    name: 'Cá',
  },
  {
    id: 5,
    specie_code: 5,
    name: 'Thỏ',
  },
  {
    id: 13,
    specie_code: 10,
    name: 'Chuột',
  },
  {
    id: 16,
    specie_code: 11,
    name: 'Sóc',
  },
  {
    id: 17,
    specie_code: 12,
    name: 'Hà Mã',
  },
  {
    id: 20,
    specie_code: 15,
    name: 'Rắn',
  },
  {
    id: 35,
    specie_code: 16,
    name: 'Tegu',
  },
  {
    id: 36,
    specie_code: 17,
    name: 'Chihuahua',
  },
  {
    id: 37,
    specie_code: 18,
    name: 'Miniature Pinscher',
  },
  {
    id: 38,
    specie_code: 19,
    name: 'Sóc',
  },
  {
    id: 39,
    specie_code: 20,
    name: 'Thằn lằn',
  },
  {
    id: 40,
    specie_code: 21,
    name: 'Nhím',
  },
  {
    id: 130,
    specie_code: 37,
    name: 'Rùa',
  },
];

export const countries = [
  {
    country_id: 220,
    name: 'Việt Nam',
  },
  {
    country_id: 1,
    name: 'Ả Rập Saudi',
  },
  {
    country_id: 2,
    name: 'Afghanistan',
  },
  {
    country_id: 3,
    name: 'Ai Cập',
  },
  {
    country_id: 4,
    name: 'Albania',
  },
  {
    country_id: 5,
    name: 'Algeria',
  },
  {
    country_id: 6,
    name: 'Ấn Độ',
  },
  {
    country_id: 7,
    name: 'Andorra',
  },
  {
    country_id: 8,
    name: 'Angola',
  },
  {
    country_id: 9,
    name: 'Anguilla',
  },
  {
    country_id: 10,
    name: 'Anh',
  },
  {
    country_id: 11,
    name: 'Antigua và Barbuda',
  },
  {
    country_id: 12,
    name: 'Áo',
  },
  {
    country_id: 13,
    name: 'Argentina',
  },
  {
    country_id: 14,
    name: 'Armenia',
  },
  {
    country_id: 15,
    name: 'Aruba',
  },
  {
    country_id: 16,
    name: 'Azerbaijan',
  },
  {
    country_id: 17,
    name: 'Ba Lan',
  },
  {
    country_id: 18,
    name: 'Bahamas',
  },
  {
    country_id: 19,
    name: 'Bahrain',
  },
  {
    country_id: 20,
    name: 'Bangladesh',
  },
  {
    country_id: 21,
    name: 'Barbados',
  },
  {
    country_id: 22,
    name: 'Belarus',
  },
  {
    country_id: 23,
    name: 'Belize',
  },
  {
    country_id: 24,
    name: 'Benin',
  },
  {
    country_id: 25,
    name: 'Bermuda',
  },
  {
    country_id: 26,
    name: 'Bhutan',
  },
  {
    country_id: 27,
    name: 'Bỉ',
  },
  {
    country_id: 28,
    name: 'Bờ Biển Ngà',
  },
  {
    country_id: 29,
    name: 'Bồ Đào Nha',
  },
  {
    country_id: 30,
    name: 'Bolivia',
  },
  {
    country_id: 31,
    name: 'Bosna và Hercegovina',
  },
  {
    country_id: 32,
    name: 'Botswana',
  },
  {
    country_id: 33,
    name: 'Brazil',
  },
  {
    country_id: 34,
    name: 'Brunei',
  },
  {
    country_id: 35,
    name: 'Bulgaria',
  },
  {
    country_id: 36,
    name: 'Burkina Faso',
  },
  {
    country_id: 37,
    name: 'Burundi',
  },
  {
    country_id: 38,
    name: 'Các tiểu vương quốc Ả rập thống nhất',
  },
  {
    country_id: 39,
    name: 'Cameroon',
  },
  {
    country_id: 40,
    name: 'Campuchia',
  },
  {
    country_id: 41,
    name: 'Canada',
  },
  {
    country_id: 42,
    name: 'Cape Verde',
  },
  {
    country_id: 43,
    name: 'CHDC Congo',
  },
  {
    country_id: 44,
    name: 'Chile',
  },
  {
    country_id: 45,
    name: 'Colombia',
  },
  {
    country_id: 46,
    name: 'Comoros',
  },
  {
    country_id: 47,
    name: 'Cộng hòa Congo',
  },
  {
    country_id: 48,
    name: 'Cộng hòa Dominica',
  },
  {
    country_id: 49,
    name: 'Cộng hòa Macedonia',
  },
  {
    country_id: 50,
    name: 'Cộng hòa Séc',
  },
  {
    country_id: 51,
    name: 'Cộng hòa Trung Phi',
  },
  {
    country_id: 52,
    name: 'Costa Rica',
  },
  {
    country_id: 53,
    name: 'Croatia',
  },
  {
    country_id: 54,
    name: 'Cuba',
  },
  {
    country_id: 55,
    name: 'Curaçao',
  },
  {
    country_id: 59,
    name: 'Djibouti',
  },
  {
    country_id: 60,
    name: 'Dominica',
  },
  {
    country_id: 57,
    name: 'Đài Loan',
  },
  {
    country_id: 56,
    name: 'Đan Mạch',
  },
  {
    country_id: 58,
    name: 'Đảo Man',
  },
  {
    country_id: 61,
    name: 'Đông Timor',
  },
  {
    country_id: 62,
    name: 'Đức',
  },
  {
    country_id: 63,
    name: 'Ecuador',
  },
  {
    country_id: 64,
    name: 'El Salvador',
  },
  {
    country_id: 65,
    name: 'Eritrea',
  },
  {
    country_id: 66,
    name: 'Estonia',
  },
  {
    country_id: 67,
    name: 'Ethiopia',
  },
  {
    country_id: 68,
    name: 'Fiji',
  },
  {
    country_id: 69,
    name: 'Gabon',
  },
  {
    country_id: 70,
    name: 'Gambia',
  },
  {
    country_id: 71,
    name: 'Ghana',
  },
  {
    country_id: 72,
    name: 'Gibraltar',
  },
  {
    country_id: 73,
    name: 'Greenland',
  },
  {
    country_id: 74,
    name: 'Grenada',
  },
  {
    country_id: 75,
    name: 'Gruzian11',
  },
  {
    country_id: 76,
    name: 'Guam',
  },
  {
    country_id: 77,
    name: 'Guatemala',
  },
  {
    country_id: 78,
    name: 'Guernsey',
  },
  {
    country_id: 79,
    name: 'Guinea',
  },
  {
    country_id: 80,
    name: 'Guinea Xích Đạo',
  },
  {
    country_id: 81,
    name: 'Guinea-Bissau',
  },
  {
    country_id: 82,
    name: 'Guyana',
  },
  {
    country_id: 83,
    name: 'Hà Lan',
  },
  {
    country_id: 84,
    name: 'Haiti',
  },
  {
    country_id: 85,
    name: 'Hàn Quốc',
  },
  {
    country_id: 86,
    name: 'Honduras',
  },
  {
    country_id: 87,
    name: 'Hồng Kông',
  },
  {
    country_id: 88,
    name: 'Hungary',
  },
  {
    country_id: 89,
    name: 'Hy Lạp',
  },
  {
    country_id: 90,
    name: 'Iceland',
  },
  {
    country_id: 91,
    name: 'Indonesia',
  },
  {
    country_id: 92,
    name: 'Iran',
  },
  {
    country_id: 93,
    name: 'Iraq',
  },
  {
    country_id: 94,
    name: 'Ireland',
  },
  {
    country_id: 95,
    name: 'Israel',
  },
  {
    country_id: 96,
    name: 'Jamaica',
  },
  {
    country_id: 97,
    name: 'Jersey',
  },
  {
    country_id: 98,
    name: 'Jordan',
  },
  {
    country_id: 99,
    name: 'Kazakhstan',
  },
  {
    country_id: 100,
    name: 'Kenya',
  },
  {
    country_id: 101,
    name: 'Kiribati',
  },
  {
    country_id: 102,
    name: 'Kuwait',
  },
  {
    country_id: 103,
    name: 'Kyrgyzstan',
  },
  {
    country_id: 104,
    name: 'Lào',
  },
  {
    country_id: 105,
    name: 'Latvia',
  },
  {
    country_id: 106,
    name: 'Lesotho',
  },
  {
    country_id: 107,
    name: 'Liban',
  },
  {
    country_id: 108,
    name: 'Liberia',
  },
  {
    country_id: 109,
    name: 'Libya',
  },
  {
    country_id: 110,
    name: 'Liechtenstein',
  },
  {
    country_id: 111,
    name: 'Liên bang Micronesia',
  },
  {
    country_id: 112,
    name: 'Liên Bang Nga',
  },
  {
    country_id: 113,
    name: 'Litva',
  },
  {
    country_id: 114,
    name: 'Luxembourg',
  },
  {
    country_id: 115,
    name: 'Macau',
  },
  {
    country_id: 116,
    name: 'Madagascar',
  },
  {
    country_id: 117,
    name: 'Malawi',
  },
  {
    country_id: 118,
    name: 'Malaysia',
  },
  {
    country_id: 119,
    name: 'Maldives',
  },
  {
    country_id: 120,
    name: 'Mali',
  },
  {
    country_id: 121,
    name: 'Malta',
  },
  {
    country_id: 122,
    name: 'Maroc',
  },
  {
    country_id: 123,
    name: 'Mauritania',
  },
  {
    country_id: 124,
    name: 'Mauritius',
  },
  {
    country_id: 125,
    name: 'Mexico',
  },
  {
    country_id: 126,
    name: 'Moldova',
  },
  {
    country_id: 127,
    name: 'Monaco',
  },
  {
    country_id: 128,
    name: 'Mông Cổ',
  },
  {
    country_id: 129,
    name: 'Montenegro',
  },
  {
    country_id: 130,
    name: 'Montserrat',
  },
  {
    country_id: 131,
    name: 'Mozambique',
  },
  {
    country_id: 133,
    name: 'Mỹ',
  },
  {
    country_id: 132,
    name: 'Myanmar',
  },
  {
    country_id: 134,
    name: 'Na Uy',
  },
  {
    country_id: 135,
    name: 'Nam Phi',
  },
  {
    country_id: 136,
    name: 'Nam Sudan',
  },
  {
    country_id: 137,
    name: 'Namibia',
  },
  {
    country_id: 138,
    name: 'Nauru',
  },
  {
    country_id: 139,
    name: 'Nepal',
  },
  {
    country_id: 140,
    name: 'New Zealand',
  },
  {
    country_id: 141,
    name: 'Nhật Bản',
  },
  {
    country_id: 142,
    name: 'Nicaragua',
  },
  {
    country_id: 143,
    name: 'Niger',
  },
  {
    country_id: 144,
    name: 'Nigeria',
  },
  {
    country_id: 145,
    name: 'Niue',
  },
  {
    country_id: 146,
    name: 'Oman',
  },
  {
    country_id: 147,
    name: 'Pakistan',
  },
  {
    country_id: 148,
    name: 'Palau',
  },
  {
    country_id: 149,
    name: 'Panama',
  },
  {
    country_id: 150,
    name: 'Papua New Guinea',
  },
  {
    country_id: 151,
    name: 'Paraguay',
  },
  {
    country_id: 152,
    name: 'Peru',
  },
  {
    country_id: 153,
    name: 'Phần Lann9',
  },
  {
    country_id: 154,
    name: 'Pháp',
  },
  {
    country_id: 155,
    name: 'Philippines',
  },
  {
    country_id: 156,
    name: 'Puerto Rico',
  },
  {
    country_id: 157,
    name: 'Qatar',
  },
  {
    country_id: 158,
    name: 'Quần đảo Bắc Mariana',
  },
  {
    country_id: 159,
    name: 'Quần đảo Cayman',
  },
  {
    country_id: 160,
    name: 'Quần đảo Cook',
  },
  {
    country_id: 161,
    name: 'Quần đảo Falkland',
  },
  {
    country_id: 162,
    name: 'Quần đảo Faroe',
  },
  {
    country_id: 163,
    name: 'Quần đảo Marshall',
  },
  {
    country_id: 164,
    name: 'Quần đảo Pitcairn',
  },
  {
    country_id: 165,
    name: 'Quần đảo Solomon',
  },
  {
    country_id: 166,
    name: 'Quần đảo Turks và Caicos',
  },
  {
    country_id: 167,
    name: 'Quần đảo Virgin thuộc Anh',
  },
  {
    country_id: 168,
    name: 'Quần đảo Virgin thuộc Mỹ',
  },
  {
    country_id: 169,
    name: 'Romania',
  },
  {
    country_id: 170,
    name: 'Rwanda',
  },
  {
    country_id: 171,
    name: 'Saint Helena, Ascension',
  },
  {
    country_id: 172,
    name: 'Saint Kitts và Nevis',
  },
  {
    country_id: 173,
    name: 'Saint Lucia',
  },
  {
    country_id: 174,
    name: 'Saint Vincent và Grenadines',
  },
  {
    country_id: 175,
    name: 'Samoa',
  },
  {
    country_id: 176,
    name: 'Samoa thuộc Mỹ',
  },
  {
    country_id: 177,
    name: 'San Marino',
  },
  {
    country_id: 178,
    name: 'São Tomé và Príncipe',
  },
  {
    country_id: 179,
    name: 'Senegal',
  },
  {
    country_id: 180,
    name: 'Serbia',
  },
  {
    country_id: 181,
    name: 'Seychelles',
  },
  {
    country_id: 182,
    name: 'Sierra Leone',
  },
  {
    country_id: 183,
    name: 'Singapore',
  },
  {
    country_id: 184,
    name: 'Sint Maarten',
  },
  {
    country_id: 185,
    name: 'Síp',
  },
  {
    country_id: 186,
    name: 'Slovakia',
  },
  {
    country_id: 187,
    name: 'Slovenia',
  },
  {
    country_id: 188,
    name: 'Somalia',
  },
  {
    country_id: 189,
    name: 'Sri Lanka',
  },
  {
    country_id: 190,
    name: 'Sudann16',
  },
  {
    country_id: 191,
    name: 'Suriname',
  },
  {
    country_id: 192,
    name: 'Swaziland',
  },
  {
    country_id: 193,
    name: 'Syria',
  },
  {
    country_id: 194,
    name: 'Tajikistan',
  },
  {
    country_id: 195,
    name: 'Tanzania',
  },
  {
    country_id: 196,
    name: 'Tây Ban Nha',
  },
  {
    country_id: 197,
    name: 'Tây Sahara',
  },
  {
    country_id: 198,
    name: 'Tchad',
  },
  {
    country_id: 199,
    name: 'Thái Lan',
  },
  {
    country_id: 200,
    name: 'Thổ Nhĩ Kỳ',
  },
  {
    country_id: 201,
    name: 'Thụy Điển',
  },
  {
    country_id: 202,
    name: 'Thụy Sĩ',
  },
  {
    country_id: 205,
    name: 'Togo',
  },
  {
    country_id: 206,
    name: 'Tokelau',
  },
  {
    country_id: 207,
    name: 'Tonga',
  },
  {
    country_id: 203,
    name: 'Triều Tiên',
  },
  {
    country_id: 208,
    name: 'Trinidad và Tobago',
  },
  {
    country_id: 204,
    name: 'Trung Quốc',
  },
  {
    country_id: 209,
    name: 'Tunisia',
  },
  {
    country_id: 210,
    name: 'Turkmenistan',
  },
  {
    country_id: 211,
    name: 'Tuvalu',
  },
  {
    country_id: 212,
    name: 'Úc',
  },
  {
    country_id: 213,
    name: 'Uganda',
  },
  {
    country_id: 214,
    name: 'Ukraina',
  },
  {
    country_id: 215,
    name: 'Uruguay',
  },
  {
    country_id: 216,
    name: 'Uzbekistan',
  },
  {
    country_id: 217,
    name: 'Vanuatu',
  },
  {
    country_id: 218,
    name: 'Vatican',
  },
  {
    country_id: 219,
    name: 'Venezuela',
  },
  {
    country_id: 221,
    name: 'Vùng lãnh thổ Palestine',
  },
  {
    country_id: 222,
    name: 'Ý',
  },
  {
    country_id: 223,
    name: 'Yemen',
  },
  {
    country_id: 224,
    name: 'Zambia',
  },
  {
    country_id: 225,
    name: 'Zimbabwe',
  },
];

export const tribes: { [id: number]: any[] } = {
  9: [
    {
      name: '111111111111111111111111111111112',
    },
    {
      name: 'Akita',
    },
    {
      name: 'Alaska',
    },
    {
      name: 'Alaska Malamute',
    },
    {
      name: 'Alaskan Husky',
    },
    {
      name: 'Ba Tư',
    },
    {
      name: 'Bắc Hà',
    },
    {
      name: 'Bắc Kinh',
    },
    {
      name: 'Bắc Kinh Lai Nhật',
    },
    {
      name: 'Beagle',
    },
    {
      name: 'Becgie',
    },
    {
      name: 'Bécgie Bỉ',
    },
    {
      name: 'Bécgie Đức',
    },
    {
      name: 'Berger lai',
    },
    {
      name: 'Bichon',
    },
    {
      name: 'Border Collie',
    },
    {
      name: 'Border Collie Lai Ta',
    },
    {
      name: 'Boston Terrier',
    },
    {
      name: 'Bull',
    },
    {
      name: 'Bull Pháp',
    },
    {
      name: 'Bull Terrier',
    },
    {
      name: 'Bulldog',
    },
    {
      name: 'Bully',
    },
    {
      name: 'Cáo',
    },
    {
      name: 'Cavalier',
    },
    {
      name: 'Chăn Cừu',
    },
    {
      name: 'Chihuahua',
    },
    {
      name: 'Chó Cỏ Lai Dachshund',
    },
    {
      name: 'Chó Lai',
    },
    {
      name: 'Cỏ',
    },
    {
      name: 'Cỏ Lai Phú Quốc',
    },
    {
      name: 'Cỏ Lùn',
    },
    {
      name: 'Cỏ Mực',
    },
    {
      name: 'Cocker',
    },
    {
      name: 'Cocker Lai',
    },
    {
      name: 'Cocker Lai Cỏ',
    },
    {
      name: 'Cocker Spaniel',
    },
    {
      name: 'Corgi',
    },
    {
      name: 'Corgi Lai',
    },
    {
      name: 'Corgi Lai Nhật',
    },
    {
      name: 'Corgi Nhật',
    },
    {
      name: 'Dachshund',
    },
    {
      name: 'Dachshund Lai',
    },
    {
      name: 'Dalmatian',
    },
    {
      name: 'Dingo Đông Dương',
    },
    {
      name: 'Dingo Lùn',
    },
    {
      name: 'Dorberman',
    },
    {
      name: 'Đốm',
    },
    {
      name: 'Đức',
    },
    {
      name: 'English Bulldog',
    },
    {
      name: 'Fox, Min Pin',
    },
    {
      name: 'Gấu Chó',
    },
    {
      name: 'Golden Lai',
    },
    {
      name: 'Golden Lai Ngao Tây Tạng',
    },
    {
      name: 'Golden Retriever',
    },
    {
      name: 'Great Dane',
    },
    {
      name: "H'mông Cộc",
    },
    {
      name: 'Heo',
    },
    {
      name: 'Hổ báo',
    },
    {
      name: 'Hổ Mang',
    },
    {
      name: 'Husky',
    },
    {
      name: 'Husky Lai Phú Quốc',
    },
    {
      name: 'Jack Russell Terrier',
    },
    {
      name: 'Japanese Chin',
    },
    {
      name: 'Labrador',
    },
    {
      name: 'Lai',
    },
    {
      name: 'Lai JAPAN',
    },
    {
      name: 'Lào',
    },
    {
      name: 'Lạp Xưởng',
    },
    {
      name: 'Lông Xù',
    },
    {
      name: 'Lundehund Mix',
    },
    {
      name: 'Malinois',
    },
    {
      name: 'Méo',
    },
    {
      name: 'Miniature Pinscher',
    },
    {
      name: 'Miniature Pinscher (Fox Min Pin)',
    },
    {
      name: 'Ngáo',
    },
    {
      name: 'Ngao Tây Tạng',
    },
    {
      name: 'Ngu',
    },
    {
      name: 'Nhật',
    },
    {
      name: 'Nhật Lai',
    },
    {
      name: 'Nhật Lai Corgi',
    },
    {
      name: 'Papillon',
    },
    {
      name: 'Pembroke Welsh Corgi',
    },
    {
      name: 'Phốc',
    },
    {
      name: 'Phốc Bỏ Túi',
    },
    {
      name: 'Phốc Hươu',
    },
    {
      name: 'Phốc Lai',
    },
    {
      name: 'Phốc Lai Bắc Kinh',
    },
    {
      name: 'Phốc Lai Poodle',
    },
    {
      name: 'Phốc Min Pin',
    },
    {
      name: 'Phốc Mini',
    },
    {
      name: 'Phốc Sóc',
    },
    {
      name: 'Phốc Sóc lai',
    },
    {
      name: 'Phốc Sóc lai Bắc Kinh',
    },
    {
      name: 'Phốc Sóc Lai Nhật',
    },
    {
      name: 'Phốc Sóc Lai Poodle',
    },
    {
      name: 'Phú Quốc',
    },
    {
      name: 'Phú Quốc Lai',
    },
    {
      name: 'Phú Quốc Lai Bécgie',
    },
    {
      name: 'Phú Quốc Lai Nhật',
    },
    {
      name: 'Pit Bull Terrier',
    },
    {
      name: 'Pitbull',
    },
    {
      name: 'Pocgi',
    },
    {
      name: 'Pomeranian',
    },
    {
      name: 'Pomeranian Teacup',
    },
    {
      name: 'Poodle',
    },
    {
      name: 'Poodle Lai',
    },
    {
      name: 'Poodle Lai Codie',
    },
    {
      name: 'Poodle Lai Nhật',
    },
    {
      name: 'Poodle Lai Phốc Sốc',
    },
    {
      name: 'Poodle Teacup',
    },
    {
      name: 'Poodle Tiny',
    },
    {
      name: 'Poodle Toy',
    },
    {
      name: 'PQ Lai Rottweiler Lai Golden',
    },
    {
      name: 'Pug',
    },
    {
      name: 'Pug Lai',
    },
    {
      name: 'Pug lai Bull Pháp',
    },
    {
      name: 'Pug Mặt Ngáo',
    },
    {
      name: 'Pug Mặt Xệ',
    },
    {
      name: 'Pug Pháp',
    },
    {
      name: 'Pug X Bull Pháp',
    },
    {
      name: 'Pugf',
    },
    {
      name: 'Rottweiler',
    },
    {
      name: 'Rottweiler lai Alaska',
    },
    {
      name: 'Samoyed',
    },
    {
      name: 'Shiba',
    },
    {
      name: 'Shiba Akita',
    },
    {
      name: 'Shih Tzu',
    },
    {
      name: 'Ta',
    },
    {
      name: 'Ta (Lông Xù)',
    },
    {
      name: 'Ta (Mực)',
    },
    {
      name: 'Ta Lai',
    },
    {
      name: 'Ta Lai Dachshund',
    },
    {
      name: 'Ta Lai Nhật',
    },
    {
      name: 'Viet Nam',
    },
    {
      name: 'Xù Lai',
    },
    {
      name: 'Yorkshire-Terrier',
    },
  ],
  8: [
    {
      name: 'Abyssinian',
    },
    {
      name: 'Alaska Malamute',
    },
    {
      name: 'Ald Xám Lông Ngắn Tai Cụp',
    },
    {
      name: 'Aln Silver Tabby Scottish Fold',
    },
    {
      name: 'Angora Thổ Nhĩ Kỳ',
    },
    {
      name: 'Anh Lông Dài',
    },
    {
      name: 'Anh Lông Dài Tai Cụp',
    },
    {
      name: 'Anh Lông Ngắn',
    },
    {
      name: 'Anh Lông Ngắn Bicolor',
    },
    {
      name: 'Anh Lông Ngắn Lai Nga',
    },
    {
      name: 'Anh Lông Ngắn Lai Scottish',
    },
    {
      name: 'Ba Tư',
    },
    {
      name: 'Ba Tư Lai Anh Lông Dài',
    },
    {
      name: 'Ba Tư lai Anh Lông Ngắn',
    },
    {
      name: 'Ba Tư Lai Tai Cụp',
    },
    {
      name: 'Ba Tư Lai Thổ Nhĩ Kỳ',
    },
    {
      name: 'Bengal',
    },
    {
      name: 'Bia đia',
    },
    {
      name: 'Bombay',
    },
    {
      name: 'British Shorthair',
    },
    {
      name: 'Brown Tabby Lông Dài',
    },
    {
      name: 'Chartreux',
    },
    {
      name: 'Châu Âu Lông Ngắn',
    },
    {
      name: 'Chausie',
    },
    {
      name: 'Cinnamon',
    },
    {
      name: 'Cộc Đuôi Nhật Bản',
    },
    {
      name: 'Con Lai Ngoại Quốc',
    },
    {
      name: 'Domestic Shorthair',
    },
    {
      name: 'Exotic',
    },
    {
      name: 'Feline',
    },
    {
      name: 'Himalaya',
    },
    {
      name: 'Hổ báo',
    },
    {
      name: 'Khao Manee',
    },
    {
      name: 'La Perm',
    },
    {
      name: 'Lai',
    },
    {
      name: 'Lai Nga',
    },
    {
      name: 'Maine Coon',
    },
    {
      name: 'Manx',
    },
    {
      name: 'Mau Ai Cập',
    },
    {
      name: 'Mèo ra',
    },
    {
      name: 'Mèo Ta',
    },
    {
      name: 'Miến Điện',
    },
    {
      name: 'Muffin',
    },
    {
      name: 'Munchkin',
    },
    {
      name: 'Munchkin Anh Lông Ngắn',
    },
    {
      name: 'Munhkin',
    },
    {
      name: 'Mướp',
    },
    {
      name: 'Mỹ Lông Ngắn',
    },
    {
      name: 'Mỹ Tai Xoắn',
    },
    {
      name: 'Nga Lông Dài',
    },
    {
      name: 'Nga xanh',
    },
    {
      name: 'Nuốt Dưa Hấu',
    },
    {
      name: 'Oriental Lông Ngắn',
    },
    {
      name: 'Ragdoll',
    },
    {
      name: 'Rừng',
    },
    {
      name: 'Rừng Nauy',
    },
    {
      name: 'Samsut',
    },
    {
      name: 'Săn Bắt Côn Trùng',
    },
    {
      name: 'Savannah',
    },
    {
      name: 'Scottish',
    },
    {
      name: 'Scottish Fold',
    },
    {
      name: 'Scottish Lai Anh Lông Dài',
    },
    {
      name: 'Scottish Lai Anh Lông Dài Trắng',
    },
    {
      name: 'Scottish Thuần Lai Ald Trắng',
    },
    {
      name: 'Siberian',
    },
    {
      name: 'Somali',
    },
    {
      name: 'Sphynx',
    },
    {
      name: 'Sphynx - Ai Cập',
    },
    {
      name: 'Ta',
    },
    {
      name: 'Ta (Mướp)',
    },
    {
      name: 'Ta (Tam Thể)',
    },
    {
      name: 'Ta (Trắng)',
    },
    {
      name: 'Ta (Vàng)',
    },
    {
      name: 'Ta Lai',
    },
    {
      name: 'Ta Lai Anh Lông Dài',
    },
    {
      name: 'Ta Lai Anh Lông Ngắn',
    },
    {
      name: 'Ta Lai Lông Ngắn',
    },
    {
      name: 'Ta Xăm Trổ, Rất Nguy Hiểm',
    },
    {
      name: 'Tabby Lông Ngắn',
    },
    {
      name: 'Tabi Việt Nam Thuần Chủng',
    },
    {
      name: 'Tai Cụp',
    },
    {
      name: 'Tam Thể',
    },
    {
      name: 'Thái',
    },
    {
      name: 'Thần Miến Điện',
    },
    {
      name: 'Tonkinese',
    },
    {
      name: 'Tortoiseshell',
    },
    {
      name: 'Toyger',
    },
    {
      name: 'Trắng',
    },
    {
      name: 'Turkish Van',
    },
    {
      name: 'Vàng',
    },
    {
      name: 'Việt Nam',
    },
    {
      name: 'Xiêm',
    },
    {
      name: 'Xiêm Lai',
    },
  ],
};

export const districts: { [id: number]: any[] } = {
  1: [
    {
      district_id: 1,
      name: 'Ba Đình',
    },
    {
      district_id: 271,
      name: 'Ba Vì',
    },
    {
      district_id: 21,
      name: 'Bắc Từ Liêm',
    },
    {
      district_id: 5,
      name: 'Cầu Giấy',
    },
    {
      district_id: 277,
      name: 'Chương Mỹ',
    },
    {
      district_id: 273,
      name: 'Đan Phượng',
    },
    {
      district_id: 17,
      name: 'Đông Anh',
    },
    {
      district_id: 6,
      name: 'Đống Đa',
    },
    {
      district_id: 18,
      name: 'Gia Lâm',
    },
    {
      district_id: 268,
      name: 'Hà Đông',
    },
    {
      district_id: 7,
      name: 'Hai Bà Trưng',
    },
    {
      district_id: 274,
      name: 'Hoài Đức',
    },
    {
      district_id: 2,
      name: 'Hoàn Kiếm',
    },
    {
      district_id: 8,
      name: 'Hoàng Mai',
    },
    {
      district_id: 4,
      name: 'Long Biên',
    },
    {
      district_id: 250,
      name: 'Mê Linh',
    },
    {
      district_id: 282,
      name: 'Mỹ Đức',
    },
    {
      district_id: 19,
      name: 'Nam Từ Liêm',
    },
    {
      district_id: 280,
      name: 'Phú Xuyên',
    },
    {
      district_id: 272,
      name: 'Phúc Thọ',
    },
    {
      district_id: 275,
      name: 'Quốc Oai',
    },
    {
      district_id: 16,
      name: 'Sóc Sơn',
    },
    {
      district_id: 3,
      name: 'Tây Hồ',
    },
    {
      district_id: 276,
      name: 'Thạch Thất',
    },
    {
      district_id: 278,
      name: 'Thanh Oai',
    },
    {
      district_id: 20,
      name: 'Thanh Trì',
    },
    {
      district_id: 9,
      name: 'Thanh Xuân',
    },
    {
      district_id: 279,
      name: 'Thường Tín',
    },
    {
      district_id: 269,
      name: 'TX Sơn Tây',
    },
    {
      district_id: 281,
      name: 'Ứng Hòa',
    },
  ],
  79: [
    {
      district_id: 785,
      name: 'Bình Chánh',
    },
    {
      district_id: 777,
      name: 'Bình Tân',
    },
    {
      district_id: 765,
      name: 'Bình Thạnh',
    },
    {
      district_id: 787,
      name: 'Cần Giờ',
    },
    {
      district_id: 783,
      name: 'Củ Chi',
    },
    {
      district_id: 764,
      name: 'Gò Vấp',
    },
    {
      district_id: 784,
      name: 'Hóc Môn',
    },
    {
      district_id: 786,
      name: 'Nhà Bè',
    },
    {
      district_id: 768,
      name: 'Phú Nhuận',
    },
    {
      district_id: 760,
      name: 'Quận 1',
    },
    {
      district_id: 771,
      name: 'Quận 10',
    },
    {
      district_id: 772,
      name: 'Quận 11',
    },
    {
      district_id: 761,
      name: 'Quận 12',
    },
    {
      district_id: 769,
      name: 'Quận 2',
    },
    {
      district_id: 770,
      name: 'Quận 3',
    },
    {
      district_id: 773,
      name: 'Quận 4',
    },
    {
      district_id: 774,
      name: 'Quận 5',
    },
    {
      district_id: 775,
      name: 'Quận 6',
    },
    {
      district_id: 778,
      name: 'Quận 7',
    },
    {
      district_id: 776,
      name: 'Quận 8',
    },
    {
      district_id: 763,
      name: 'Quận 9',
    },
    {
      district_id: 766,
      name: 'Tân Bình',
    },
    {
      district_id: 767,
      name: 'Tân Phú',
    },
    {
      district_id: 762,
      name: 'Thủ Đức',
    },
  ],
};

export const communes: { [id: number]: any[] } = {
  1: [
    {
      commune_id: 7,
      name: 'Cống Vị',
    },
    {
      commune_id: 19,
      name: 'Điện Biên',
    },
    {
      commune_id: 22,
      name: 'Đội Cấn',
    },
    {
      commune_id: 31,
      name: 'Giảng Võ',
    },
    {
      commune_id: 28,
      name: 'Kim Mã',
    },
    {
      commune_id: 8,
      name: 'Liễu Giai',
    },
    {
      commune_id: 16,
      name: 'Ngọc Hà',
    },
    {
      commune_id: 25,
      name: 'Ngọc Khánh',
    },
    {
      commune_id: 10,
      name: 'Nguyễn Trung Trực',
    },
    {
      commune_id: 1,
      name: 'Phúc Xá',
    },
    {
      commune_id: 13,
      name: 'Quán Thánh',
    },
    {
      commune_id: 34,
      name: 'Thành Công',
    },
    {
      commune_id: 4,
      name: 'Trúc Bạch',
    },
    {
      commune_id: 6,
      name: 'Vĩnh Phúc',
    },
  ],
};
