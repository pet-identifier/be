FROM node:18-alpine AS node-base
WORKDIR /app
COPY package*.json ./
COPY . .
RUN yarn set version stable
RUN yarn
RUN yarn build
ENV PORT=3000
EXPOSE $PORT
CMD ["yarn", "start:prod"]

